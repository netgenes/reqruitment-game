# Simple Game (recruitment)


## start
```sh
npm install && npm run start:dev
```
## Overview 

todo

## Components

### Action Flow

Action Flow component is 'Redux' like state handler. 

### Loading Monitor

Collects information about registered responses and notify loading progress

```js
const loadingMonitor = new LoadingMonitor();

loadingMonitor.subscribe('progress', (progress) => {
    
   console.log(progress); // => [0:1]
});

fetch('/anyUrl')
.then( loadingMonitor.registerResponse )
.then( /* do what you need */)

```


### Progress Circle
todo

### Audio Manager
todo
### Circle Select

##