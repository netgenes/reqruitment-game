// @flow

// this is very simple polyfill for fetch API covering GET
// only for demo purpose, do not use it on production!

type FetchOptions = {
    method: 'string',
    headers: Headers

}

class Response {

    _req: XMLHttpRequest;
    _res: Promise<XMLHttpRequest>;
    _resolveRes: (data:any) => void;
    _rejectRes: (error:any) => void;

    constructor( request: XMLHttpRequest ) {

        this._req = request;

        //noinspection JSUndefinedPropertyAssignment
        request.onload = this._onLoad.bind(this);
        //noinspection JSUndefinedPropertyAssignment
        request.onerror = this._onError.bind(this);

        this._res = new Promise( (resolve, reject) => {

            this._resolveRes = resolve;
            this._rejectRes = reject;
        });
    }

    _onLoad( ): void {

        this._resolveRes(
            this._req
        )
    }

    _onError( ): void {

        this._rejectRes(
            this._req
        )
    }

    json(): Promise<any> {

        return this._res.then( xhr => JSON.parse( xhr.responseText ));
    }

    text(): Promise<string> {

        return this._res.then( xhr => xhr.responseText );
    }

    arrayBuffer(): Promise<ArrayBuffer> {

        this._req.responseType = 'arraybuffer';
        return this._res.then( xhr => xhr.response );
    }
}

export const fetch = ( url: string, options?: FetchOptions ): Promise<Response> => {

    return new Promise( ( resolve, reject ) => {

        try {

            let req = new XMLHttpRequest();
            let response = new Response(req);

            req.open('GET', url, true);
            req.send(null);

            resolve( response );

        } catch ( error ) {

            reject( error );
        }
    })
};


export const polyfillFeature = ( ): void => {

    if (!('fetch' in window)) {

        window.fetch = fetch;
    }
};