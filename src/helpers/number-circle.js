"use strict";
// @flow
export const numberCircle = (min: number, max: number) =>
    (value: number): number => {

        let newValue = value;

        if (newValue < min) {
            newValue = max - ( min - newValue );
        }
        else if (newValue > max) {
            newValue = min + ( newValue - max );
        }
        return newValue;
    };