// @flow
type Listener = (any) => any;
export default class EventEmitter {

    _listeners: { [string]: Listener[] };

    constructor() {
        this._listeners = {};
    }

    subscribe( eventName: string, listener: Listener ) {

        if(!this._listeners[eventName]) {
            this._listeners[eventName] = [];
        }
        this._listeners[eventName].push(listener);
    }
    notify( eventName: string, ...args: any[] ): void {

        if (!this._listeners[eventName]) {
            return;
        }
        this._listeners[eventName].forEach( listener => listener.apply(null,args));
    }
}