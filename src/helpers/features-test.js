const FEATURES = {
    passiveEvent: null
};

const passiveEvent = () => {

    if (FEATURES.passiveEvent === null) {
        FEATURES.passiveEvent = false;
        let options = {
            get passive() {
                FEATURES.passiveEvent = true;
                return true;
            }
        };
        window.addEventListener('test', () => {}, options);
    }
    return FEATURES.passiveEvent;
};

export default {
    get passiveEvent() {
        return passiveEvent();
    }
};
