// @flow
export type RGB = {
    r: number,
    g: number,
    b: number
};

export const transitionRGB = (
    startColor: RGB,
    endColor: RGB,
    progress: number
): RGB  => {

    let rDiff = endColor.r - startColor.r;
    let gDiff = endColor.g - startColor.g;
    let bDiff = endColor.b - startColor.b;

    return {
        r: Math.round(startColor.r + rDiff * progress),
        g: Math.round(startColor.g + gDiff * progress),
        b: Math.round(startColor.b + bDiff * progress)
    }
};

export const hexStringToRgb = ( hex: string ): RGB => {

    let chunkSize = hex.length === 4 ? 1 : 2;
    let regExp = new RegExp(`[0-9a-f]{${chunkSize}}`,'ig');
    // $FlowFixMe
    let chunks: string[] = hex.match(regExp);

    if (chunks.length) {
        let mapped = chunks
            .map( channel => channel.length === 1 ? channel + channel : channel)
            .map( channel => parseInt(channel,16));

        return {
            r: mapped[0],
            g: mapped[1],
            b: mapped[2]
        }
    } else {
        throw Error(`Incorrect hex color notation: (${hex})`);
    }
};

export const rgbToRgbString = ( c: RGB ): string => `rgb(${c.r},${c.g},${c.b})`;