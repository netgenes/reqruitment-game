// @flow
import { numberCircle } from './number-circle';

export type RectTuple = [number,number,number,number];

export const TAU = Math.PI * 2;

// export const radToDeg = ( radians: number): number => radians * 180 / Math.PI;

export const degToRad = ( degrees: number): number => degrees * Math.PI / 180;

export const rotateRadians = ( value: number, radians: number ):number =>
    numberCircle(0,TAU)( value + radians);

export const rotateRadByDeg = ( value: number, degrees: number ):number =>
    rotateRadians( value, degToRad(degrees) );

export const xyInRectTuple = ( x: number, y: number, rect: RectTuple ): boolean => {
    return x >= rect[0]
        && x <= rect[0] + rect[2]
        && y >= rect[1]
        && y <= rect[1] + rect[3]
};
