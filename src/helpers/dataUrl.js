// @flow
export const objectURLFromAB = (arrayBuffer: ArrayBuffer, mimeType: string): string => {
    return URL.createObjectURL(new Blob([arrayBuffer],{type:mimeType}));
};

export const getMimeType = ( url: string ): string => {

    let extensionMimeTypeMap = {
        'jpg': 'image/jpg',
        'png': 'image/png',
        'mp3': 'audio/mp3',
        'wav': 'audio/wav'
    };
    let extension = /\.(\w+)$/i.exec(url)[1];

    return extensionMimeTypeMap[extension] || 'application/octet-stream';
};
