// @flow
/* eslint-disable no-console */
import {bootstrap} from './app/bootstrap';

window.onload = bootstrap;


const initServiceWorker = ( ) => {

    if (navigator.serviceWorker) {

        navigator
            .serviceWorker
            .register('./sw.js')
            .then( () => console.log('[SW:registered]'));
    }
};

initServiceWorker();