// @flow
import { audioManager, loadingMonitor } from './singletons';

export const loadSounds = ( ): Promise<any> => {

    //noinspection ES6ModulesDependencies
    return Promise.all([
        fetch('assets/click.wav')
            .then( loadingMonitor.registerResponse )
            .then( response => response.arrayBuffer())
            .then( arrayBuffer => audioManager.registerBuffer('click', arrayBuffer)),
        fetch('assets/good.wav')
            .then( loadingMonitor.registerResponse )
            .then( response => response.arrayBuffer())
            .then( arrayBuffer => audioManager.registerBuffer('good', arrayBuffer)),
        fetch('assets/bad.wav')
            .then( loadingMonitor.registerResponse )
            .then( response => response.arrayBuffer())
            .then( arrayBuffer => audioManager.registerBuffer('bad', arrayBuffer))
    ]);
};