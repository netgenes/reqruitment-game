import { assetDataUrls } from './singletons';

export const setupViewport = (width,height) => {

    let canvas = document.querySelector('canvas');
    let viewport = document.querySelector('.mobile-viewport').getBoundingClientRect();
    canvas.width = width || viewport.width;
    canvas.height = height || viewport.height;
};

export const constraintViewport = () => {

    document.querySelector('.mobile-viewport').classList.add('mobile-viewport--landscape');
    document.querySelector('.orientation-clue').style.backgroundImage = `url(${assetDataUrls.rotate})`;
};

