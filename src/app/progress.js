// @flow
import { loadingMonitor } from './singletons';
import ProgressCircle from '../components/ProgressCircle';
let progressCircle;

export const showLoadingProgress = ( ): void => {
// $FlowFixMe
    progressCircle = new ProgressCircle( document.querySelector('canvas') );
    loadingMonitor.subscribe('progress', progressCircle.setValue.bind(progressCircle) )
};

export const hideLoadingProgress = ( ): void => {

    progressCircle.close();
};