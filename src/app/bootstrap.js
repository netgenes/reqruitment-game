import { startGame } from './game';

import { showLoadingProgress, hideLoadingProgress } from './progress';
import { setupViewport, constraintViewport } from './viewport';
import { loadAssets } from './assets.loader';
import { loadSymbols } from './symbols.loader';
import { loadSounds } from './sounds.loader';

export const bootstrap = ( ) => {

    setupViewport();
    showLoadingProgress();

    Promise.all([
        loadSymbols(),
        loadAssets(),
        loadSounds()
    ])
        .then( () => constraintViewport() )
        .then( () => hideLoadingProgress() )
        .then( () => startGame() );
};