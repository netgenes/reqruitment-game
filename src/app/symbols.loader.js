// @flow
import { store, loadingMonitor } from './singletons';
import { objectURLFromAB } from '../helpers/dataUrl';
import { load } from './state';



const allSymbols = list => {

    //noinspection ES6ModulesDependencies
    let promises = list.map(

        symbol => fetch('symbols/'+symbol.url)
            .then( loadingMonitor.registerResponse )
            .then( res => res.arrayBuffer())
            .then( arrayBuffer => Object.assign({
                    dataUrl: objectURLFromAB(arrayBuffer,'image/png')
                }, symbol)
            )
    );
    return Promise.all(promises);
};

export const loadSymbols = ( ): Promise<any> => {

    return fetch('symbols/index.json')
        .then( response => response.json())
        .then( allSymbols )
        .then( symbolsImages => store.dispatch(load(symbolsImages)));
};