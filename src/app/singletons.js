import LoadingMonitor from '../components/LoadingMonitor';
import AudioManager from '../components/AudioManager';
import createStore from '../components/ActionFlow';
import updater from './state';


export const loadingMonitor = new LoadingMonitor();
export const audioManager = new AudioManager();
export const store = createStore(updater);
export const assetDataUrls = {};

window.store = store;