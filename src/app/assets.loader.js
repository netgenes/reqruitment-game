// @flow
import { loadingMonitor, assetDataUrls } from './singletons';
import { getMimeType, objectURLFromAB } from '../helpers/dataUrl';

export const loadAssets = ( ): Promise<any> => {

    let assetIdUrlMap = {
        bg: 'assets/myBG.jpg',
        rotate: 'assets/phone_rotate-512.png',
        good: 'assets/good_mini.png',
        bad: 'assets/bad_mini.png',
        question: 'assets/qmark.png',
        refresh: 'assets/refresh.png'
    };

    let promises = [];

    Object.keys(assetIdUrlMap).forEach( key => {
        let url = assetIdUrlMap[key];
        let mimeType = getMimeType( url );

        //noinspection ES6ModulesDependencies
        let promise = fetch(assetIdUrlMap[key])
            .then(loadingMonitor.registerResponse)
            .then( response => response.arrayBuffer() )
            .then( arrayBuffer => assetDataUrls[key] = objectURLFromAB(arrayBuffer,mimeType));
        promises.push( promise );
    });

    return Promise.all(promises);
};