// @flow
"use strict";

export type GameSymbol = {

    url: string,
    name: string,
    id: number,
    dataUrl: string
};

type State = {

    symbols: {
        byId: {[id:number]: GameSymbol},
        allIds: number[]
    },
    currentSymbol: ?number,
    chosenSymbol: ?number,
    gamePhase: ?string
};

type Action<T> = {

    type: string,
    payload: T
}

const defaultState : State = {

    symbols: {
        byId: {},
        allIds: []
    },
    currentSymbol: null,
    currentSymbolVisibility: false,
    chosenSymbol: null,
    gamePhase: null
};

const LOAD = 'symbols/load';
const SELECT = 'symbols/select';
const CHANGE_CURRENT = 'symbols/change_current';
const CHANGE_PHASE = 'game/change_phase';

export const load = ( payload: GameSymbol[]): Action<GameSymbol[]> => ({type: LOAD, payload});
export const select = ( payload: number ): Action<number> => ({type: SELECT, payload});
export const changePhase = ( payload: string): Action<string>  => ({type: CHANGE_PHASE, payload });
export const changeCurrent = ( payload: number ): Action<number> => ({type: CHANGE_CURRENT, payload});

export const loadReducer = ( state: State, action: Action<GameSymbol[]> ): State => {

    let allIds = [];
    let byId = {};

    action.payload.forEach( symbol => {

        byId[ symbol.id ] = symbol;
        allIds.push( symbol.id );
    });

    return Object.assign({},state,{
        symbols: {
            allIds,
            byId
        }
    })
};

export const selectReducer = ( state: State, action: Action<number> ): State => {
    return Object.assign({},state,{
        chosenSymbol: action.payload
    });
};

export const currentReducer = ( state: State, action: Action<number>): State => {
    return Object.assign({},state,{
        currentSymbol: action.payload
    });
};

export const phaseReducer = ( state: State, action: Action<string>): State => {
    return Object.assign({},state,{
        gamePhase: action.payload
    });
};

export default ( state: State = defaultState, action: Action<any> ): State => {

    switch (action.type) {

        case LOAD: return loadReducer(state,action);
        case SELECT: return selectReducer(state,action);
        case CHANGE_CURRENT: return currentReducer(state,action);
        case CHANGE_PHASE: return phaseReducer(state,action);
        default: return state;
    }
}

export const listSymbols = (state: State): GameSymbol[] => state.symbols.allIds.map( id => state.symbols.byId[id]);
