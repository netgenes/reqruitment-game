// @flow
import { store, audioManager, assetDataUrls } from './singletons';
import { listSymbols, select, changePhase, changeCurrent } from './state';
import CircleSelect from '../components/CircleSelect';
import GameTable from '../components/GameTable';
import { setupViewport } from './viewport';

const getRandomSymbolId = ( ): number => {
    let list = store.getState().symbols.allIds;

    return list[ Math.floor( Math.random() * list.length)];
};

export const startGame = () => {
    let selectElement: ?HTMLElement = document.querySelector('[data-circle-select]');
    let canvasElement: ?HTMLCanvasElement = document.querySelector('canvas');

    const circleSelect = new CircleSelect(
        selectElement,
        listSymbols( store.getState())
    );
    const gameTable = new GameTable(
        canvasElement,
        {
            bg: assetDataUrls.bg,
            qMark: assetDataUrls.question,
            good: assetDataUrls.good,
            bad: assetDataUrls.bad,
            refresh: assetDataUrls.refresh
        }
    );

    circleSelect.onChange = ( value: any ) => {
        store.dispatch(select( value.id ));
    };
    circleSelect.onTick = ( ) => audioManager.play('click');

    gameTable.subscribe('check', () => {
        let state = store.getState();
        let nextPhase = state.currentSymbol === state.chosenSymbol ? 'good' : 'bad';
        store.dispatch(changePhase(nextPhase));
    });
    gameTable.subscribe('refresh', () => {
        store.dispatch(changePhase('covered'));
        store.dispatch(changeCurrent(getRandomSymbolId()));
    });

    store.subscribe( ( state, oldState ) =>  {

        if (!oldState && state.symbols.allIds || (oldState.symbols.allIds !== state.symbols.allIds)) {

            circleSelect.updateOptions( listSymbols(state) );
        }

        if (!oldState && state.currentSymbol || (oldState.currentSymbol !== state.currentSymbol)) {

            gameTable.setSymbol( state.symbols.byId[state.currentSymbol]);
        }

        if (!oldState && state.gamePhase || (oldState.gamePhase !== state.gamePhase)) {

            gameTable.setState( state.gamePhase );

            if (state.gamePhase === 'good') {
                audioManager.play('good');
            }
            else if (state.gamePhase === 'bad') {
                audioManager.play('bad');
            }
        }
    });

    store.dispatch(changeCurrent(getRandomSymbolId()));
    store.dispatch(select(circleSelect.value.id));

    window.addEventListener('orientationchange', () => {
        // $FlowFixMe
        if (screen.orientation.type.indexOf('land') > -1) {

            setTimeout(() => {
                setupViewport();
                gameTable._initialRender();
            },100);
        }
    })
};