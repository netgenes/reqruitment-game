"use strict";
// @flow

const APP_VERSION : string = '1.0.4';
const CACHED_ASSETS: Array<any> = [
    '/.',
    '/bundle.js',
    '/bundle.js.map',
    '/favicon-16x16.png',
    '/assets/bad_mini.png',
    '/assets/bad.wav',
    '/assets/click.wav',
    '/assets/good_mini.png',
    '/assets/good.wav',
    '/assets/myBG.jpg',
    '/assets/phone_rotate-512.png',
    '/assets/qmark.png',
    '/assets/refresh.png',
    '/symbols/index.json',
    '/symbols/SYM1.png',
    '/symbols/SYM3.png',
    '/symbols/SYM4.png',
    '/symbols/SYM5.png',
    '/symbols/SYM6.png',
    '/symbols/SYM7.png'
];

const onInstall = (event: ExtendableEvent):void => {

    //noinspection ES6ModulesDependencies,ES6ModulesDependencies
    event.waitUntil(
        caches
            .open(APP_VERSION)
            .then(
                cache => cache.addAll( CACHED_ASSETS )
            )

    );

    console.log('[SW:installed]');
};

const onFetch = (event: FetchEvent):void => {

    event.respondWith(
        caches.match(event.request)
            .then(function(response) {
                    // Cache hit - return response
                    if (response) {
                        return response;
                    }
                    //noinspection ES6ModulesDependencies
                return fetch(event.request);
                }
            )
    )
};

self.addEventListener('activate', (event) => {

    event.waitUntil(
        caches.keys().then(function(cacheNames) {
            return Promise.all(
                cacheNames.map(function(cacheName) {
                    if (cacheName !== APP_VERSION) {
                        return caches.delete(cacheName);
                    }
                })
            );
        })
    );
});
self.addEventListener('install',onInstall);
self.addEventListener('fetch', onFetch );