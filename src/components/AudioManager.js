// @flow
export default class Manager {
    _sounds: { [id:string]: HTMLMediaElement };
    _buffers: { [id:string]: AudioBuffer };
    _context: AudioContext;
    constructor( ) {
        this._sounds = {};
        this._buffers = {};
        this._context = new AudioContext();
    }
    registerBuffer( id: string, buffer: ArrayBuffer ): void {

        this._context.decodeAudioData(
            buffer,
            audioBuffer => this._buffers[id] = audioBuffer,
            error => console.error(error)
        )
    }
    play( id: string ) {

        let buffer = this._buffers[id];
        let source = this._context.createBufferSource();
        //
        source.buffer = buffer;
        source.connect(this._context.destination);
        source.start(0);
    }
}