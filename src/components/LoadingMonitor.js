// @flow
import EventEmitter from '../helpers/EventEmitter';

export default class LoadingMonitor extends EventEmitter{
    _expected: number;
    _loaded: number;
    _done: boolean;
    constructor() {
        super();
        this._expected = 0;
        this._loaded = 0;
        this._done = false;
        // $FlowFixMe
        this.registerResponse = this.registerResponse.bind(this);
    }

    registerResponse( response: Response ): Response {

        let contentLength = response.headers.get('content-length');
        if (contentLength) {
            this._expected += +contentLength;
            this._handleResponse( response, +contentLength );
        }

        return response;
    }
    getPercentage01Range(): number {
        if (this._expected ) {
            return this._loaded / this._expected;
        }
        return 0;
    }
    _handleResponse( response: Response, size: number ) {
        // i need to wait for pipeTrough on readable stream in fetch API
        // or for FetchObserver
        // for achieve smooth byte accurate progress notification
        // till time i just notice every finished response
        // $FlowFixMe
        if (response.body.getReader) {
            this._withClone(response);
        }
        else {
            this._withoutClone(response,size);
        }

    }
    _notifyProgress( ) {
        if (this._done) {
            return;
        }
        let progress = this.getPercentage01Range();
        this.notify('progress', progress );
        if (1 === progress) {
            this._done = true;
            this.notify('done', true);
        }
    }
    _withClone( response: Response ) {
        // $FlowFixMe
        let reader =  response.clone().body.getReader();

        let read = () => reader.read().then(result => {

            this._loaded += result.value ? result.value.length : 0;
            this._notifyProgress();
            if (!result.done) {
                read();
            }
        });
        read();
    }
    _withoutClone( response: Response, size: number) {

        response.arrayBuffer()
            .then( () => this._loaded += size)
            .then( () => this._notifyProgress());
    }
}