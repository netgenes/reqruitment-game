// @flow

import type { GameSymbol } from '../app/state';
import { numberCircle } from '../helpers/number-circle';
import { identity } from '../helpers/identity';
import { TAU } from '../helpers/geometry';
import Features from '../helpers/features-test';

const radianCircle = numberCircle( 0, TAU );


export default class CircleSelect {

    options: GameSymbol[];
    value: GameSymbol;
    onChange: ( GameSymbol ) => any;
    onTick: () => any;
    _ref: Element;

    _currentTouch: Touch;
    _spin: number;
    _radius: number;
    _animation: boolean;


    constructor( _ref: ?Element, options: GameSymbol[] = []) {
        if (!_ref) {
            throw new Error('Circle Select ref not specified');
        }
        this._ref = _ref;
        this._spin = 0;
        this._radius = 250;
        this._animation = false;
        this.onChange = identity;
        this.onTick = identity;

        this.updateOptions( options );
    }

    updateOptions( options: GameSymbol[] ): void {

        this.options = options;
        this.value = options[0];
        this._renderContent();
        this._renderState();

        if (this.value) {

            this.onChange( this.value );
        }
    }

    handleTouchStart( event: TouchEvent ): void {

        this._currentTouch = event.touches[0];
        this._animation = true;

        this._animate();
    }

    handleTouchEnd( ) {
        this._snapToCurrentValue();
    }

    handleTouchMove( event: TouchEvent): void {

        let touch = event.touches[0];
        let delta = {
            x : this._currentTouch.clientX - touch.clientX,
            y : this._currentTouch.clientY - touch.clientY
        };

        this._currentTouch = touch;

        this._spin = radianCircle( this._spin + Math.PI / (this._radius * 2 / delta.y));

        let howClose = this._spin % (TAU / this.options.length);

        if ( 0.1 > howClose  ) {

            let newValue = this._getValueBySpin();
            if (newValue !== this.value) {
                this.value = newValue;
                this.onChange(newValue);
                this.onTick();
            }
        }
    }

    handleClick( event: MouseEvent ) {
        // $FlowFixMe
        let optionId = event.target.parentNode.getAttribute('data-option-id');

        let option = this.options[optionId];

        if (this.value !== option) {
            this.value = option;
            this.onChange(option);
            this._animation = true;
            this._snapToCurrentValue().then( this.onTick );
            this._animate();
        }
    }

    _getListElement(): Element {
        // $FlowFixMe
        return this._ref.querySelector('ul');
    }

    _bindOptionListeners( element: Element ): void {

        if ('ontouchstart' in window) {
            element.addEventListener('touchstart', this.handleTouchStart.bind(this));
            element.addEventListener('touchmove', this.handleTouchMove.bind(this), Features.passiveEvent ? { passive: true } : true );
            element.addEventListener('touchend', this.handleTouchEnd.bind(this));
            element.addEventListener('tauchcancel', console.log.bind(console,'touch cancel'));

        } else {

            element.addEventListener('click', this.handleClick.bind(this));
        }
    }

    _createOptionNode( option: GameSymbol ): HTMLElement {

        let optionElement = document.createElement('li');
        optionElement.setAttribute('class', 'circle-select__option');
        optionElement.setAttribute('data-option-id', this.options.indexOf(option).toString());

        let img = document.createElement('img');
        img.src = option.dataUrl;

        optionElement.appendChild(img);

        return optionElement;
    }

    _renderContent(): void {

        let list: Element = this._getListElement();

        list.innerHTML = '<!-- empty -->';
        this.options
            .map( this._createOptionNode.bind(this) )
            .forEach( optionNode => {
                list.appendChild(optionNode);
                this._bindOptionListeners( optionNode );
            });
    }

    _renderState(): void {

        let optionElements = this._ref.querySelectorAll('.circle-select__option');

        let tc = TAU / optionElements.length;

        let rootElementClientRect = this._ref.getBoundingClientRect();


        let cX = rootElementClientRect.left;
        let cY = rootElementClientRect.top;


        optionElements.forEach( (nodeElement, index) => {

            let rad = tc * index + this._spin;
            let tX = cX - (this._radius * Math.cos(rad) + cX);
            let tY = cY - (this._radius * Math.sin(rad) + cY);

            nodeElement.style.transform = `
            translate(${tX}px,${tY}px) rotate(${rad}rad)
            `;
        });
    }

    _animate( ) {
        this._renderState();
        if (this._animation) {
            requestAnimationFrame(this._animate.bind(this));
        }
    }

    _snapToCurrentValue( ): Promise<any> {
        return new Promise(
            ( resolve ) => {

                let transitionTime = 200;
                let frameTime = 16;

                let currentIndex = this.options.length - this.options.indexOf(this.value);
                let expectedSpin = radianCircle( (TAU / this.options.length) * currentIndex);

                let spinDiff = expectedSpin -this._spin;

                let frames = transitionTime / frameTime;
                let spinPerFrame = spinDiff / frames;
                let time = 0;

                let interval = setInterval( () => {
                    this._spin = radianCircle(this._spin + spinPerFrame);
                    time += 16;
                    if (time >= transitionTime) {
                        clearInterval(interval);
                        resolve(true);
                        this._spin = expectedSpin;
                        this._animation = false;
                    }
                }, 16 );
            }
        )
    }

    _getValueBySpin( ) {

        let ol = this.options.length;

        let is = TAU / ol;

        let i = ol - Math.round(this._spin / is);

        return this.options[i < ol ? i : 0];
    }
}
