// @flow
import type { RGB } from '../helpers/color';
import { hexStringToRgb, rgbToRgbString, transitionRGB } from '../helpers/color';
import { TAU, rotateRadByDeg } from '../helpers/geometry';

type ProgressCircleOptions = {
    circleWidth: number,
    startColor: string,
    endColor: string
}
const defaultOptions: ProgressCircleOptions = {

    circleWidth: 20,
    startColor: '#6464ff',
    endColor: '#64ff64'
};

export default class ProgressCircle {
    value: number;
    _canvas: HTMLCanvasElement;
    _ctx: CanvasRenderingContext2D;
    _options: ProgressCircleOptions;
    _width: number;
    _height: number;
    _cx: number;
    _cy: number;
    _radius: number;
    _animation: boolean;
    _closed: boolean;
    _startColor: RGB;
    _endColor: RGB;
    _color: string;
    constructor( canvas: HTMLCanvasElement, options: ProgressCircleOptions = defaultOptions ) {

        this.value = 0;
        this._options = options;
        this._canvas = canvas;
        this._width = canvas.width;
        this._height = canvas.height;
        this._cx = this._width * 0.5;
        this._cy = this._height * 0.5;
        this._closed = false;
        this._radius = Math.round( Math.min( this._width, this._height) * 0.35);
        // $FlowFixMe
        this._ctx = canvas.getContext('2d');
        this._animation = true;
        this._startColor = hexStringToRgb(options.startColor);
        this._endColor = hexStringToRgb(options.endColor);
        this._color = rgbToRgbString(this._startColor);
        this._animate();
    }
    setValue( value: number ) {
        if ( value > 1 || value < 0) {
            throw new RangeError(`Value (${value}) is out od range [0:1];`);
        }
        this._color = rgbToRgbString(
            transitionRGB(
                this._startColor,
                this._endColor,
                this.value
            )
        );
        this.value = value;
    }
    close( ) {
        this._closed = true;
        this._ctx.clearRect(...this._getUsedRect());
    }

    _render() {
        if (this._closed) {
            return;
        }
        let startAngle = rotateRadByDeg(0, -90);
        let endAngle = rotateRadByDeg(TAU * this.value, -90);

        if(startAngle === endAngle) {
            endAngle -= 0.01;
        }

        this._ctx.clearRect(...this._getUsedRect());

        // circle
        if (this.value > 0) {
            this._ctx.beginPath();
            this._ctx.strokeStyle = this._color;
            this._ctx.lineWidth = 20;
            this._ctx.arc(
                this._cx,
                this._cy,
                this._radius,
                startAngle,
                endAngle
            );
            if( this.value === 1 ) {
                this._ctx.closePath();
            }
            this._ctx.stroke();
        }

        // labels
        this._ctx.fillStyle = '#666';
        this._ctx.font = `${this._radius * 0.5}px Arial`;
        this._ctx.textAlign = 'center';
        this._ctx.textBaseline = 'middle';
        this._ctx.fillText( this._getHumanReadablePercentage(), this._cx, this._cy);

    }
    _animate() {
        this._render();

        if(this._animation) {

            requestAnimationFrame(this._animate.bind(this));
        }
    }
    _getUsedRect():any {
        let width = this._options.circleWidth;
        return [
            this._cx - this._radius - width,
            this._cy - this._radius - width,
            this._cx + this._radius + width,
            this._cy + this._radius + width
        ]
    }
    _getHumanReadablePercentage(): string {
        return `${Math.round(this.value * 100)} %`;
    }
}