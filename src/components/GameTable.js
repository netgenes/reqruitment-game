// @flow
import EventEmitter from '../helpers/EventEmitter';
import { xyInRectTuple} from '../helpers/geometry';
type GameTableOptions = {
    bg: string,
    good: string,
    bad: string,
    qMark: string
}

type ImgRecord = {
    key: string,
    img: Image
}

type ImgMap = {
    [id:string]: Image
}

import type { RectTuple } from '../helpers/geometry';
import type { GameSymbol } from '../app/state';

export default class GameTable extends EventEmitter {
    currentSymbol: GameSymbol;
    state: string;
    _canvas: HTMLCanvasElement;
    _ctx: CanvasRenderingContext2D;
    _images: ImgMap;
    _currentSymbolImage: Image;
    _refreshAreas: RectTuple[];
    _playAgainArea: RectTuple;
    _checkArea: RectTuple;
    constructor( canvasElement: ?HTMLCanvasElement, options: GameTableOptions ) {
        super();
        if (canvasElement instanceof HTMLCanvasElement) {

            this._canvas = canvasElement;
        }
        // $FlowFixMe
        this._ctx = canvasElement.getContext('2d');
        this._loadImages(options)
            .then( () => this._initialRender() );

        this._bindListeners();
        this.state = 'covered';
    }
    setSymbol( symbol: GameSymbol) {

        this.currentSymbol = symbol;

        let imgId = 'symbol_'+symbol.id;

        if (this._images[imgId]) {
            this._currentSymbolImage = this._images[imgId];
        } else {
            this.__waitForImage(symbol.dataUrl).then( img => {

                this._currentSymbolImage = img;
                this._images[imgId] = img;
            })
        }
    }
    setState( state: string ) {

        this.state = state;
        this._render();
    }
    _drawPlaceholder() {

        let imgToDraw = this._images.qMark;
        let affectedArea = [
            20,
            this._getVerticallyCenteredY(imgToDraw),
            imgToDraw.width,
            imgToDraw.height
        ];
        this._checkArea = affectedArea;
        this._ctx.drawImage( imgToDraw, ...affectedArea );
        this._refreshAreas.push(affectedArea);
    }
    _drawSymbol() {

        let imgToDraw = this._currentSymbolImage;
        let affectedArea = [
            50,
            this._getVerticallyCenteredY(imgToDraw),
            imgToDraw.width,
            imgToDraw.height
        ];
        this._ctx.drawImage( imgToDraw, ...affectedArea );
        this._refreshAreas.push(affectedArea);

    }
    _drawResult( ) {

        // refresh
        let refreshImg = this._images.refresh;
        let refreshAffectedArea = [
            this._getHorizontallyCenteredX(refreshImg),
            this._translateBottomY(refreshImg,10),
            refreshImg.width,
            refreshImg.height
        ];
        this._playAgainArea = refreshAffectedArea;
        this._ctx.drawImage(refreshImg, ...refreshAffectedArea );

        let resultImg = this._images[this.state] || this._images.good;
        let resultAffectedArea = [
            this._getHorizontallyCenteredX(resultImg),
            10,
            resultImg.width,
            resultImg.height
        ];

        this._ctx.drawImage(resultImg, ...resultAffectedArea );
        this._refreshAreas.push(refreshAffectedArea);
        this._refreshAreas.push(resultAffectedArea);

    }
    _initialRender() {
        this._refreshAreas = [[ 0, 0, this._canvas.width, this._canvas.height ]];
        this._render();
    }
    _render( ) {

        // background
        let bgImg = this._images.bg;
        this._ctx.save();
        this._ctx.globalAlpha = 1;

        this._refreshAreas.forEach( refreshArea => {
            this._ctx.drawImage( bgImg, ...refreshArea, ...refreshArea )
        });
        this._refreshAreas = [];
        this._ctx.restore();

        if ('covered' !== this.state) {

            this._drawSymbol();
            this._drawResult();
        }
        else {
            this._drawPlaceholder();
        }
    }
    handleClick( event: MouseEvent ) {
        // $FlowFixMe
        let x = event.layerX;
        // $FlowFixMe
        let y = event.layerY;

        if (this._playAgainArea && xyInRectTuple(x, y, this._playAgainArea)) {

            this.notify('refresh');
        }
        else if (this._checkArea && xyInRectTuple(x, y, this._checkArea)) {

            this.notify('check');
        }
    }
    _bindListeners( ) {

        this._canvas.addEventListener('click', this.handleClick.bind(this))
    }


    _loadImages( options: GameTableOptions ) {

        this._images = {};

        let promises: Promise<ImgRecord>[] = Object.keys(options).map( key => {

            return this.__waitForImage( options[key] ).then( img => ({key, img}) )
        });

        return Promise.all( promises )
            .then( (images: ImgRecord[]) => {

                images.forEach( imcRec => this._images[imcRec.key] = imcRec.img);
                return true;
            })
    }
    __waitForImage( imgDataUrl: string ): Promise<Image> {

        let img = new Image();
        img.src = imgDataUrl;

        return new Promise( (resolve, reject) => {
            img.onload = () => resolve(img);
            img.onerror = (e) => reject(e);
        })
    }
    _getVerticallyCenteredY( img: Image ) {

        return (this._canvas.height - img.height) / 2;
    }
    _getHorizontallyCenteredX( img: Image) {

        return (this._canvas.width - img.width) / 2;
    }
    _translateBottomY( img: Image, value: number ) {

        return this._canvas.height - (img.height + value );
    }
}
