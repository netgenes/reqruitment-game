// @flow

export type Subscriber<S> = (newState: S, oldState: S) => void;
export type Action<P> = {
    type: string,
    payload?: P
}

type Updater<S,A:Action<any>> = ( state: S, action: A ) => S;
type Dispatch<A:Action<any>> = ( action: A) => A;


class Store<S, A:Action<any>> {

    _state: S;
    _updater: ( S, A ) => S;
    _subscribers: Array<Subscriber<S>>;

    dispatch: Dispatch<A>;
    constructor( updater: Updater<S,A>) {

        this._updater = updater;
        this._subscribers = [];
        this.dispatch = this.dispatch.bind(this);
    }

    getState( ): S {

        return this._state;
    }

    dispatch( action: A )  {
        let newState = this._updater( this._state, action );
        let oldState = this._state;

        if (newState !== this._state) {
            this._state = newState;
            this._subscribers.forEach( subscriber => subscriber(newState, oldState));
        }

        return action;
    }

    subscribe( subscriber: Subscriber<S> ): void {

        this._subscribers.push(subscriber);
    }
}

function createStore<S, A:Action<any>>( updater: Updater<S,A> ): Store<S,A> {

    let store: Store<S,A> = new Store( updater );
    // $FlowFixMe
    store.dispatch({type:'@init'});

    return store;
}

export default createStore;