'use strict';
const CopyPlugin = require('webpack-copy-plugin');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const path = require('path');

const PUBLIC_PATH = path.join(__dirname,'dist/public');

module.exports = [
    {
        name: 'browser',
        entry: './src/browser.js',
        output: {
            path: PUBLIC_PATH,
            filename: 'bundle.js'
        },
        devtool: 'source-map',
        module: {
            loaders: [
                { test: /\.js$/, exclude: /node_modules/, loader: 'babel-loader'}
            ]
        },
        plugins: [
            new HtmlWebpackPlugin({
                template: 'src/index.html'
            }),
            new CopyPlugin({dirs:[
                { from: 'src/symbols', to: PUBLIC_PATH+'/symbols' },
                { from: 'src/assets', to: PUBLIC_PATH+'/assets' },
                { from: 'src/icons', to: PUBLIC_PATH }
            ]
            }),
            new BrowserSyncPlugin({
                host: 'localhost',
                port: 3030,
                server: { baseDir: ['dist/public'] }
            })
        ]
    }
    ,{
        name: 'serviceWorker',
        entry: './src/sw.js',
        output: {
            path: PUBLIC_PATH,
            filename: 'sw.js'
        },
        module: {
            loaders: [
                { test: /\.js$/, exclude: /node_modules/, loader: 'babel-loader'}
            ]
        }
    }
];